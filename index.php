<?php include_once('lib/includes.php'); ?>
<?php include_once('includes/header.php'); ?>

<body>
	<div class="estrutura">
		<header class="header">
			<a href="#" class="logo"><img src="images/logo.png" alt="Animes no Sekai"></a>
			<nav>
				<ul>
					<li><a href="#">Início</a></li>
					<li><a href="#">Lançamentos</a></li>
					
				</ul>
			</nav>
		</header>
		<nav>
			<div class="widgetTitulo luffy">
				<span style="font-size: 20px;bottom: 9px;left: 20px">Gêneros!</span>
			</div>
			<nav class="sidenav" id="scrollIn">
				<ul>
					<li class="item"><a href="#">Ação</a></li>
					<li class="item"><a href="#">Adaptação de Manga</a></li>
					<li class="item"><a href="#">Animação</a></li>
					<li class="item"><a href="#">Artes Marciais</a></li>
					<li class="item"><a href="#">Aventura</a></li>
					<li class="item"><a href="#">Bishounen</a></li>
					<li class="item"><a href="#">Comédia</a></li>
					<li class="item"><a href="#">Comédia Romântica</a></li>
					<li class="item"><a href="#">Demônios</a></li>
					<li class="item"><a href="#">Drama</a></li>
					<li class="item"><a href="#">Ecchi</a></li>
					<li class="item"><a href="#">Escolar</a></li>
					<li class="item"><a href="#">Esporte</a></li>
					<li class="item"><a href="#">Fantasia</a></li>
					<li class="item"><a href="#">Ficção Científica</a></li>
					<li class="item"><a href="#">Harém</a></li>
					<li class="item"><a href="#">Histórico</a></li>
					<li class="item"><a href="#">Horror</a></li>
					<li class="item"><a href="#">Jogos</a></li>
					<li class="item"><a href="#">Magia</a></li>
					<li class="item"><a href="#">Mecha</a></li>
					<li class="item"><a href="#">Militar</a></li>
					<li class="item"><a href="#">Mistério</a></li>
					<li class="item"><a href="#">Musical</a></li>
				</ul>
			</nav>
		</nav>	
		<main class="content">
			<div class="titulo">
				<span class="col-wide">Últimos Lançamentos! - Animes Online</span>
			</div>
			<div class="conteudoBlog">
				
				<section class="animes">
					<article>
						<img class="thumb" src="https://animesonlinebr.co/thumbs/tower-of-god-2020-04-01-04-31.jpg">
						<p>Tower of God - Episódio 01</p>
						<button><a href="#">Assistir</a></button>
					</article>
					<article>
						<img src="https://animesonlinebr.co/thumbs/tower-of-god-2020-04-01-04-31.jpg">
						<p>Tower of God - Episódio 02</p>
						<button><a href="#">Assistir</a></button>
					</article>
					<article>
						<img src="https://animesonlinebr.co/thumbs/tower-of-god-2020-04-01-04-31.jpg">
						<p>Tower of God - Episódio 03</p>
						<button><a href="#">Assistir</a></button>
					</article>
					<article>
						<img src="https://animesonlinebr.co/thumbs/tower-of-god-2020-04-01-04-31.jpg">
						<p>Tower of God - Episódio 04</p>
						<button><a href="#">Assistir</a></button>
					</article>
					<article>
						<img src="https://animesonlinebr.co/thumbs/tower-of-god-2020-04-01-04-31.jpg">
						<p>Tower of God - Episódio 05</p>
						<button><a href="#">Assistir</a></button>
					</article>
					<article>
						<img src="https://animesonlinebr.co/thumbs/tower-of-god-2020-04-01-04-31.jpg">
						<p>Tower of God - Episódio 06</p>
						<button><a href="#">Assistir</a></button>
					</article>
				</section>
			</div>
		</main>
		



<?php include_once('includes/footer-site.php'); ?>