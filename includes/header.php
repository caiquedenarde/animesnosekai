<!DOCTYPE html>
<html>
<head>
	<title><?php echo titulo_site; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<base href="<?php echo url_site; ?>">
	<link href="https://fonts.googleapis.com/css2?family=Vollkorn:ital,wght@0,400;0,700;1,400&display=swap" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/style-site.css">
	<link rel="stylesheet" type="text/css" href="css/style-adm.css">
	<link rel="stylesheet" type="text/css" href="css/responsivo.css" media="screen">
</head>
